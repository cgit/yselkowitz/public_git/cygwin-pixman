%{?cygwin_package_header}

Name:           cygwin-pixman
Version:        0.34.0
Release:        1%{?dist}
Summary:        Cygwin pixel manipulation library

License:        MIT
Group:          Development/Libraries
URL:            http://cgit.freedesktop.org/pixman/
BuildArch:      noarch

Source0:        http://xorg.freedesktop.org/archive/individual/lib/pixman-%{version}.tar.bz2
Patch0:         pixman-0.20.0-gcc4-visibility.patch

BuildRequires:  automake autoconf libtool

BuildRequires:  cygwin32-filesystem
BuildRequires:  cygwin32-binutils
BuildRequires:  cygwin32-gcc
BuildRequires:  cygwin32

BuildRequires:  cygwin64-filesystem
BuildRequires:  cygwin64-binutils
BuildRequires:  cygwin64-gcc
BuildRequires:  cygwin64


%description
Cygwin pixel manipulation library for X and cairo.

%package -n cygwin32-pixman
Summary:        Cygwin64 pixel manipulation library
Group:          Development/Libraries
Provides:       %{name} = %{version}-%{release}
Obsoletes:      %{name} < %{version}-%{release}

%description -n cygwin32-pixman
Pixel manipulation library for Cygwin i686 toolchain.

%package -n cygwin64-pixman
Summary:        Cygwin64 pixel manipulation library
Group:          Development/Libraries

%description -n cygwin64-pixman
Pixel manipulation library for Cygwin x86_64 toolchain.

%{?cygwin_debug_package}


%prep
%setup -q -n pixman-%{version}
%patch0 -p2
autoreconf -fiv


%build
%cygwin_configure --disable-static
%cygwin_make %{?_smp_mflags}


%install
%cygwin_make install DESTDIR=$RPM_BUILD_ROOT

# We intentionally do not ship *.la files
find $RPM_BUILD_ROOT -name '*.la' -delete


%files -n cygwin32-pixman
%{cygwin32_bindir}/cygpixman-1-0.dll
%{cygwin32_includedir}/pixman-1/
%{cygwin32_libdir}/libpixman-1.dll.a
%{cygwin32_libdir}/pkgconfig/pixman-1.pc

%files -n cygwin64-pixman
%{cygwin64_bindir}/cygpixman-1-0.dll
%{cygwin64_includedir}/pixman-1/
%{cygwin64_libdir}/libpixman-1.dll.a
%{cygwin64_libdir}/pkgconfig/pixman-1.pc


%changelog
* Mon Sep 12 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 0.34.0-1
- new version

* Thu Mar 31 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 0.32.8-1
- new version

* Tue Mar 10 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 0.32.6-1
- new version

* Thu Jan 23 2014 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 0.32.4-1
- Version bump.

* Tue Jul 02 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 0.28.2-1
- Version bump.
- Adapt to new Cygwin packaging scheme.
- Add cygwin64 package.

* Wed Nov 07 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 0.28.0-1
- Version bump.

* Wed Oct 24 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 0.26.2-1
- Version bump.

* Wed May 30 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 0.24.4-1
- Version bump.

* Mon Mar 14 2011 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 0.20.2-1
- Initial RPM release.
